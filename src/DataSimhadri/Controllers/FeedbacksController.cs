using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using DataSimhadri.Models;

namespace DataSimhadri.Controllers
{
    public class FeedbacksController : Controller
    {
        private AppDbContext _context;

        public FeedbacksController(AppDbContext context)
        {
            _context = context;    
        }

        // GET: Feedbacks
        public IActionResult Index()
        {
            return View(_context.Feedbacks.ToList());
        }

        // GET: Feedbacks/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Feedback feedback = _context.Feedbacks.Single(m => m.Feedback_ID == id);
            if (feedback == null)
            {
                return HttpNotFound();
            }

            return View(feedback);
        }

        // GET: Feedbacks/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Feedbacks/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Feedback feedback)
        {
            if (ModelState.IsValid)
            {
                _context.Feedbacks.Add(feedback);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(feedback);
        }

        // GET: Feedbacks/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Feedback feedback = _context.Feedbacks.Single(m => m.Feedback_ID == id);
            if (feedback == null)
            {
                return HttpNotFound();
            }
            return View(feedback);
        }

        // POST: Feedbacks/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Feedback feedback)
        {
            if (ModelState.IsValid)
            {
                _context.Update(feedback);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(feedback);
        }

        // GET: Feedbacks/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Feedback feedback = _context.Feedbacks.Single(m => m.Feedback_ID == id);
            if (feedback == null)
            {
                return HttpNotFound();
            }

            return View(feedback);
        }

        // POST: Feedbacks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Feedback feedback = _context.Feedbacks.Single(m => m.Feedback_ID == id);
            _context.Feedbacks.Remove(feedback);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
