﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace DataSimhadri.Models
{
    public class Feedback
    {
            [ScaffoldColumn(false)]
           [Key]
            public int Feedback_ID { get; set; }

            [Display(Name = "Feedback Score")]
            public string feedbackscore { get; set; }

            public string CourseID { get; set; }
            public string SectionID { get; set; }
            public string Faculty_ID { get; set; }
            public string Student_ID { get; set; }


       
    }
}
